> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 - Advanced Mobile Application Development

## Enrique Toledo

### Project 1 Requirements:

*Six Parts:*

1. Create My Music app that plays and pauses music using MediaPlayer
2. Create Splashscreen
3. Include working audio on My Music app that user can control
6. Provide screenshots of skillsets 7 - 9 
7. Questions Ch. 7 & 8

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshotofrunning application’s splash screen;
* Screenshotofrunning application’s follow-up screen(with images and buttons);
* Screenshots of running application’s play and pause user interfaces (with images and buttons);
* Screenshots of Skill Sets 7 - 9

> #### Git commands w/short descriptions:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:
 
| *Screenshot of Splash Screen*:                 | *Screenshot of Playing Screen*:                  |
|------------------------------------------------|--------------------------------------------------|
| ![Screenshot of Splash Screen](img/splash.png) | ![Screenshot of Playing Screen](img/playing.png) |

| *Screenshot of Opening Screen*:                  | *Screenshot of Pause Screen*:                 |
|--------------------------------------------------|-----------------------------------------------|
| ![Screenshot of Opening Screen](img/opening.png) | ![Screenshot of Pause Screen](img/paused.png) |

| *Screenshots of Skill Set 7 - Measurement Conversion*: |                                       |
|--------------------------------------------------------|---------------------------------------|
| ![Screenshot of ss7_1](img/ss7_1.png)                  | ![Screenshot of ss7_2](img/ss7_2.png) |


| *Screenshots of Skill Set 8 - Distance Calculator (GUI)*:    |                                      |
|--------------------------------------------------------------|--------------------------------------|
| ![Screenshot of ss8](img/ss8_1.png)                          | ![Screenshot of ss8](img/ss8_62.png) |
|--------------------------------------------------------------|--------------------------------------|

| *Screenshots of Skill Set 9 - Multiple Selection Lists(GUI)*: |                                       |
|---------------------------------------------------------------|---------------------------------------|
| ![Screenshot of ss9](img/ss9_1.png)                           | ![Screenshot of ss9](img/ss9_2.png)   |
|---------------------------------------------------------------|---------------------------------------|
| ![Screenshot of ss9](img/ss9_3.png)                           |                                       |
|---------------------------------------------------------------|---------------------------------------|

