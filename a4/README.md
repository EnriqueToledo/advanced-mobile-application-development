> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 - Advanced Mobile Application Development

## Enrique Toledo

### Assignment 4 Requirements:

*Seven Parts:*

1. Create Home Mortgage Calculator
2. Include splash screen image(or, create your own), app title, intro text.
3. Include appropriateimages.
4. Must use persistent data: SharedPreferences
4. Widgets and images must be vertically and horizontally aligned.
5. Must add background color(s) or theme
6. Create and displaylauncher icon image

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen;
* Screenshot of running application’s invalid screen(with appropriate image);
* Screenshots of running application’s valid screen(with appropriate image);
* Screenshots of Skill Sets 4 - 6

> #### Git commands w/short descriptions:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:
 
| *Screenshot of Splash Screen*:                       | *Screenshot of Main Screen*:               |
|------------------------------------------------------|--------------------------------------------|
| ![Screenshot of Splash Screen](img/splash.png)       | ![Screenshot of main screen](img/main.png) |

| *Screenshot of Incorrect number of years*:          | *Screenshot of Valid Entry*:          |
|-----------------------------------------------------|---------------------------------------|
| ![Screenshot of incorrect years](img/incorrect.png) | ![Screenshot of valid](img/valid.png) |

| *Screenshots of Skill Set 10 - Travel Time*: | *Screenshots of Skill Set 11 - Product Class*: |
|----------------------------------------------|------------------------------------------------|
| ![Screenshot of ss10](img/ss10.png)          | ![Screenshot of ss11](img/ss11.png)            |

| *Screenshots of Skill Set 12 - Book Inherets Product Class*: |
|--------------------------------------------------------------|
| ![Screenshot of ss12](img/ss12.png)                           |

