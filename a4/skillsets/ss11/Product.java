package ss11;

public class Product {
    
    private String code;
    private String description;
    private double price;

    public Product()
    {
        System.out.println("\nInside product default constructor.");

        code = "abc123";
        description = "My Widget";
        price = 49.99;
    }

    public Product(String code, String description, double price)
    {
        System.out.println("\nInside product default constructor with parameters.");
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String co)
    {
        code = co;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String de)
    {
        description = de;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double pr)
    {
        price = pr;
    }

    public void print()
    {
        System.out.println("\nCode: " + code + ", Description: " + description + ", Price: $" + price);
    }
}
