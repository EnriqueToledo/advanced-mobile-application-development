package ss12;

import java.util.Scanner;

public class BookDemo 
{
        public static void main(String[] args)
        {
            String co = "";
            String de = "";
            double pr = 0.0;
            String a = "";

            Scanner sc = new Scanner(System.in);

            System.out.println("\n/////Below are base class constructor values://///");
            Product v1 = new Product();
            System.out.println("\nCode = " + v1.getCode());
            System.out.println("\nDescription = " + v1.getDescription());
            System.out.println("\nPrice = $" + v1.getPrice());
    
            System.out.println("\n/////Below are *base class* user_entered values (instantiating p2, then using getter methods)://///");

            System.out.print("Code: ");
            co = sc.nextLine();

            System.out.print("Description: ");
            de = sc.nextLine();

            System.out.print("Price: ");
            pr = sc.nextDouble();

            Product v2 = new Product(co, de, pr);
            System.out.println("Code = " + v2.getCode());
            System.out.println("description = " + v2.getDescription());
            System.out.println("Price = $" + v2.getPrice());

            System.out.println("\n/////Below using setter methods to pass literal values to p2, then print() method to display values://///");
            v2.setCode("xyz789");
            v2.setDescription("Test Widget");
            v2.setPrice(89.99);
            v2.print();

            System.out.println();

            System.out.print("\n/////Below are derived class default constructor values (instantiating b1, then using getter methods)://///");
            Book b1 = new Book();
            System.out.println("\nCode = " + b1.getCode());
            System.out.println("\nDescription = " + b1.getDescription());
            System.out.println("\nPrice = " + b1.getPrice());
            System.out.println("\nAuthor = " + b1.getAuthor());

            System.out.println("\nOr...");
            b1.print();

            System.out.print("\n/////Below are *derived class* user-entered values (instantiating b2, then using getter methods)://///");

            System.out.print("\nCode: ");
            co = sc.next();

            System.out.print("\nDescription: ");
            de = sc.next();

            System.out.print("\nPrice: ");
            pr = sc.nextDouble();

            System.out.print("\nAuthor: ");
            a = sc.next();

            Book b2 = new Book(co,de,pr,a);
            System.out.print("\nCode = " + b2.getCode());
            System.out.print("\nDescription = " + b2.getDescription());
            System.out.print("\nPrice = " + b2.getPrice());
            System.out.print("\nAuthor = " + b2.getAuthor());

            System.out.println("\nOr...");
            b2.print();



        }
}

