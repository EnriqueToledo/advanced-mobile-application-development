package ss12;

class Book extends Product
{
    private String author;

    public Book()
    {

    super();
    System.out.println("\nInside product default constructor.");

    author = "Jane Doe";

    }

    public Book(String co, String de, double pr, String a)
    {
        super(co, de, pr);
        System.out.println("\nInside employee constructor with parameters.");

        author = a;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String a)
    {
        author = a;
    }

    public void print()
    {
        super.print();
        System.out.print(", Author: " + author);
    }

}