package ss12;

import java.util.Scanner;

public class PersonDemo 
{
    public static void main(String[] args)
    {
        String co = "";
        String de = "";
        double pr = 0.0;
        Scanner sc = new Scanner(System.in);

        System.out.println("\n/////Below are default constructor values://///");
        Product v1 = new Product();
        System.out.println("\nCode = " + v1.getCode());
        System.out.println("\nDescription = " + v1.getDescription());
        System.out.println("\nPrice = $" + v1.getPrice());

        System.out.println("\n/////Below are user-entered values://///");

        System.out.print("Code: ");
        co = sc.nextLine();

        System.out.print("Description: ");
        de = sc.nextLine();

        System.out.print("Price: ");
        pr = sc.nextDouble();

        Product v2 = new Product(co, de, pr);
        System.out.println("Code = " + v2.getCode());
        System.out.println("description = " + v2.getDescription());
        System.out.println("Price = $" + v2.getPrice());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
        v2.setCode("xyz789");
        v2.setDescription("Test Widget");
        v2.setPrice(89.99);
        v2.print();


    }
}
