> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 - Advanced Mobile Application Development

## Enrique Toledo

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Course title, your name, assignment requirements, as per A1;
    - Screenshot of running application’s unpopulated user interface;
    - Screenshot of running application’s populated user interface;
    - Screenshots of skillsets 1 - 3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Currency Calculator application
    - Create splash screen
    - Convert US dollars to Euros, Pesos, & Canadian
    - Change application theme
    - Create textfield for users to input dollar amount they want to convert

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Music app that plays and pauses music using MediaPlayer
    - Create Splashscreen
    - Include working audio on My Music app that user can control

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Mortgage Calculator app
    - Create Splashscreen
    - Modify images on app based on user input
    - Skill Sets 10 - 12

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create News Reader
    - Import RSS feed
    - Access RSS feed items
    - provide informatino for RSS item
    - Create Read more link to full article

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create my user application
    - Include splash screen.
    - Must use persistent data: SQLite database.
    - Insert at least five users.
    - Must add background color(s) or theme.
    - Create and display launcher icon image.
