> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 - Advanced Mobile Application Development

## Enrique Toledo

### Assignment 2 Requirements:

*Six Parts:*

1. Create Tip Calculator application using Android Studio
2. Screenshots of functional Tip Calculator application
3. Change Application background
4. Create Launcher Tip Calculator launcher icon
5. Provide screenshots of skillsets 1 - 3
6. Questions Ch. 3 & 4

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s populated user interface;
* Screenshots of skillsets 1 - 3

> #### Git commands w/short descriptions:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:

| *Screenshot of running unpopulated Tip Calculator Application*:               | *Screenshot of running populated Tip Calculator Application*:               |
|-------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| ![Screenshot of running unpopulated Tip Calculator Application](img/app1.png) | ![Screenshot of running populated Tip Calculator Application](img/app2.png) |

| *Screenshot of Skill Set 1 - Non-OOP Circle*: |
|-----------------------------------------------|
| ![Skill Set 1 Screenshot](img/ss1.png)        | 

| *Screenshot of Skill Set 2 - Multiple Number*: | *Screenshot of Skill Set 3 - Nested Structures 2*: |
|------------------------------------------------|----------------------------------------------------|
| ![Skill Set 2 Screenshot](img/ss2.png)         | ![Skill Set 3 Screenshot](img/ss3.png)             |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/EnriqueToledo/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")