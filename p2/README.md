> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 - Advanced Mobile Application Development

## Enrique Toledo

### Project 2 Requirements:

*Six Parts:*

1. Create my user application
2. Include splash screen.
3. Must use persistent data: SQLite database.
4. Insert at least five users.
5. Must add background color(s) or theme.
6. Create and display launcher icon image.

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of running Splash screen;
* Screenshot of running Add User;
* Screenshots of running Update User;
* Screenshot of running View Users;
* Screenshots of running Delete User;

> #### Git commands w/short descriptions:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:
 
| *Screenshot of Splash Screen*:                 | *Screenshot of Add User*:              | *Screenshot of Update User*:                 |
|------------------------------------------------|----------------------------------------|----------------------------------------------|
| ![Screenshot of Splash Screen](img/splash.png) | ![Screenshot of Add User](img/add.png) | ![Screenshot of Update User](img/update.png) |

| *Screenshot of View Users*:               | *Screenshot of Delete User*:                 |
|-------------------------------------------|----------------------------------------------|
| ![Screenshot of View Users](img/view.png) | ![Screenshot of Delete User](img/delete.png) |


