import java.util.Scanner;

public class TimeConversion 
{
    public static void main (String[]args)
    {
        System.out.println("Program converts seconds to minutes, hours, days, weeks, and (regular) years--365.");
        System.out.println("***Notes***: \n1) Use integer for seonds (must validate integer input). \n2) Use printf() function to print (format values per below input).");
        System.out.println("2) Use printf() function to print (format values per below output).");
        System.out.println("3) Create Java \"constants\" for the following values: \n\tSECS_IN_MINS, \n\tMINS_IN_HR, \n\tHRS_IN_DAY, \n\tDAYS_IN_WEEK, \n\tDAYS_IN_YR (365 days)\n");

        int seconds=0;
        double minutes=0.0;
        double hours=0.0;
        double days=0.0;
        double weeks=0.0;
        double years=0.0;

        final double SECS_IN_MINS = 60;
        final double MINS_IN_HR = 60;
        final double HRS_IN_DAY = 24;
        final double DAYS_IN_WEEK = 7;
        final double DAYS_IN_YEAR = 365;

        Scanner input = new Scanner(System.in);

        System.out.print("Please enter number of seconds: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();
            System.out.print("Please enter number of seconds: ");
        }
        
        seconds = input.nextInt();

        minutes = seconds / SECS_IN_MINS;
        hours = minutes / MINS_IN_HR;
        days = hours / HRS_IN_DAY;
        weeks = days / DAYS_IN_WEEK;
        years = days / DAYS_IN_YEAR;

        System.out.printf("%,d second(s) equals\n\n%,.2f minute(s)\n%,.3f hours(s)\n%,.4f day(s)\n%,.5f week(s)\n%,.6f year(s)\n", seconds, minutes, hours, days, weeks, years);

        
    }
    
}
