> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 - Advanced Mobile Application Development

## Enrique Toledo

### Assignment 3 Requirements:

*Six Parts:*

1. Create Currency Calculator application
2. Create spalsh screen
3. Convert US dollars to Euros, Pesos, & Canadian
4. Change application theme
5. Create textfield for users to input dollar amount they want to convert
6. Provide screenshots of skillsets 4 - 6
7. Questions Ch. 5 & 6

#### README.md file should include the following items:

* Screenshot of running Currency Converter splash screen
* Screenshot of running Currency Converter unpopulated user interface
* Screenshot of running Currency Converter toast notification
* Screenshot of running Currency Converter converted currency user interface
* Screenshots of Skill Sets 4 - 6

> #### Git commands w/short descriptions:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:
 
| *Screenshot of Splash Screen*:                       | *Screenshot of unpopulated UI*:                      |
|------------------------------------------------------|------------------------------------------------------|
| ![Screenshot of Splash Screen](img/splashscreen.png) | ![Screenshot of unpopulated UI](img/unpopulated.png) |

| *Screenshot of Toast Notification*:                | *Screenshot of converted currency*:                    |
|----------------------------------------------------|--------------------------------------------------------|
| ![Screenshot of Toast Notification](img/toast.png) | ![Screenshot of converted currency](img/converted.png) |

| *Screenshots of Skill Set 4 - Time Conversion*: |                                       |
|-------------------------------------------------|---------------------------------------|
| ![Screenshot of ss4_1](img/ss4_1.png)           | ![Screenshot of ss4_2](img/ss4_2.png) |

| *Screenshots of Skill Set 5 - Even or Odd:    |                                       |
|-----------------------------------------------|---------------------------------------|
| ![Screenshot of ss5](img/ss5_5.png)           | ![Screenshot of ss5](img/ss5_6.png)   |
|-----------------------------------------------|---------------------------------------|
| ![Screenshot of ss5](img/ss5_7.png)           | ![Screenshot of ss5](img/ss5_2.png)   |
|-----------------------------------------------|---------------------------------------|
| ![Screenshot of ss5](img/ss5_3.png)           |                                       |

| *Screenshots of Skill Set 6 - Paint Calculator: |                                       |
|-------------------------------------------------|---------------------------------------|
| ![Screenshot of ss6](img/ss6_1.png)             | ![Screenshot of ss6](img/ss6_2.png)   |
|-------------------------------------------------|---------------------------------------|
| ![Screenshot of ss6](img/ss6_3.png)             | ![Screenshot of ss6](img/ss6_4.png)   |
|-------------------------------------------------|---------------------------------------|
| ![Screenshot of ss6](img/ss6_5.png)             | ![Screenshot of ss6](img/ss6_6.png)   |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/EnriqueToledo/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")