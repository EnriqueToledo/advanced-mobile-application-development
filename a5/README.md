> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 - Advanced Mobile Application Development

## Enrique Toledo

### Assignment 5 Requirements:

*Seven Parts:*

1. Create News Reader
2. Import RSS feed
3. Access RSS feed items
4. provide informatino for RSS item
5. Create Read more link to full article

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of running Items screenshot;
* Screenshot of running Item screenshot;
* Screenshots of running Read more screenshot;
* Screenshots of Skill Sets 13 - 15

> #### Git commands w/short descriptions:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:
 
| *Screenshot of Items activity*:                | *Screenshot of Item activity*:               | *Screenshot of Read more...*:                |
|------------------------------------------------|----------------------------------------------|----------------------------------------------|
| ![Screenshot of Items activity](img/items.png) | ![Screenshot of Item activity](img/item.png) | ![Screenshot of Read more](img/readmore.png) |

| *Screenshot of ss13*:               |
|-------------------------------------|
| ![Screenshot of ss13](img/ss13.png) |

| *Screenshot of ss14*:               |
|-------------------------------------|
| ![Screenshot of ss14](img/ss14.png) |

| *Screenshot of ss15*:               |
|-------------------------------------|
| ![Screenshot of ss15](img/ss15.png) |

