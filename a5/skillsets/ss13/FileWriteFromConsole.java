package ss13;

import java.io.*;
import java.util.Scanner;

public class FileWriteFromConsole 
{
    public static void main(String [] args)
    {
        String myFile = "filewrite.txt";

        Scanner sc = new Scanner(System.in);
        File outFile = new File(myFile);

        if (outFile.length() > 0)
        {
            System.out.print("Overwrite existing file (y/n)? ");
            if (!sc.nextLine().startsWith("y"))
            {
                return;
            }
            else
            {
                writeFile(sc, outFile);
            }
        }
        else
        {
            writeFile(sc, outFile);
        }
    }

    static void writeFile(Scanner scan, File myFile)
    {
        try
        {
            PrintWriter pw = new PrintWriter(myFile);

            System.out.println("Please enter text, and hit \"Enter\" key twice when finished: ");

            while(true)
            {
                String line = scan.nextLine();
                pw.println(line);

                if (line.equals(""))
                {
                    System.out.println("File is complete!");
                    break;
                }
            }

            scan.close();
            pw.close();
        }

        catch(IOException ex)
        {
            System.out.println("Error writing to file " + myFile + "");
        }
    }
    
}
