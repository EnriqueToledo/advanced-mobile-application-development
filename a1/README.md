> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 - Advanced Mobile Application Development

## Enrique Toledo

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (1-2)
4. Bitbucket repo link to this assignment

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screnshot of running Android Studio - My First App
* Screenshot of running Android Studio - Contacts App

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one.
2. git status - Show the working tree status.
3. git add - Add file content to the index.
4. git commit - Record changes to the repository.
5. git push - Update remote refs along with associated objects.
6. git pull - Fetch from and integrate with another repository or a local branch.
7. git merge - Join two or more development histories together.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.


#### Assignment Screenshots:

*Screenshot of running java Hello*:

![Screenshot of running java Hello](img/helloworld.png)

| *Screenshot of Android Studio - My First App*: |                                                  |
|------------------------------------------------|--------------------------------------------------|
| ![My First App Screenshot](img/firstapp_1.png) | ![My First App Screenshot 2](img/firstapp_2.png) |

| *Screenshot of Android Studio - Contacts App*:  |                                                     |
|-------------------------------------------------|-----------------------------------------------------|
| ![Contacts App Screenshot](img/contactsapp.png) | ![Contacts App Screenshot 2](img/andrewcontact.png) |

| *Screenshot of Android Studio - Contacts App cont'd*: |                                                      |
|-------------------------------------------------------|------------------------------------------------------|
| ![Contacts App Screenshot 3](img/tylercontact.png)    | ![Contacts App Screenshot 4](img/devontecontact.png) |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/EnriqueToledo/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")